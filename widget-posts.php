<?php
class CH_posts_widget extends WP_Widget {
	function __construct() {
		$widget_ops = array( 
			'classname' => 'ch-thumb-post-widget',
		);
		parent::__construct( 'ch-thumb-post-widget', 'Niroon Posts Widget', $widget_ops );
        }

	public function widget( $args, $instance ) {
        $ch_slider_post_num   = isset($instance['ch-widget-post-number']) ? $instance['ch-widget-post-number'] : 3;
        $ch_show_sticky_posts = isset($instance['ch-widget-post-is-allow-sticky']) ? 0 : 1 ;
        $query_params = array(
            'post_type' => 'post',
            'orderby' => 'date',
            'order' => 'DESC',
            'posts_per_page' => $ch_slider_post_num,
            'ignore_sticky_posts' =>$ch_show_sticky_posts
    );
    ?>
    <div class = 's-one-widget '>
        <div class = 'widget-content st-posts-widget'>
        <?php if(strlen($instance['ch-widget-title']) > 0) : ?>
           <span class = 's-widget-title'><?php echo esc_html($instance['ch-widget-title']);?></span>
        <?php else: ?>
           <span class = 's-widget-title'><?php esc_html_e('Latest Posts','niroon');?></span>
        <?php endif ; ?>
             <ul>
               <?php  $ch_wg_load_posts = new WP_query($query_params); ?>
                  <?php if ( $ch_wg_load_posts->have_posts() ) : ?> 
                    <?php while ( $ch_wg_load_posts->have_posts() ) : $ch_wg_load_posts->the_post(); ?>
                    <li id="post-<?php the_ID(); ?>" <?php post_class('ch-widget-single-post-th'); ?>>  
                        <?php if(has_post_thumbnail()):?>
                           <span class = 'ch-widget-thumb'><?php the_post_thumbnail('ch-widget-thumbnail');?></span>
                        <?php else : ?>
                           <span class = 'ch-widget-thumb'><img src = '<?php echo WP_PLUGIN_URL. "/niroon-core/images/noimage.jpg" ; ?>' /></span>
                        <?php endif ; ?>
                       
                        <span class = 'ch-widget-content'>
                        <?php if(strlen(get_the_title()) > 0) : ?>
                            <?php the_title("<a class = 'ch-custom-widget-post-permalink' href ='".esc_url( get_permalink() )."'>" , "</a>") ; ?>
                        <?php else : ?>
                            <a href = '<?php esc_url(the_permalink()); ?>' class = 'ch-custom-widget-post-permalink'><?php esc_html_e('No Title','niroon') ; ?></a>
                        <?php endif ; ?>
                        <span class = 'ch-custom-widget-post-date'><?php echo get_the_date() ; ?></span>
                      
                    </span><!-- Widget Container -->
                    </li>
                <?php wp_reset_postdata() ; endwhile ; endif; ?>
            </ul>
        </div>
    </div><!-- end widget -->
    <?php
    }
	public function form( $instance ) {
        $ch_widget_title      = isset($instance['ch-widget-title']) ? $instance['ch-widget-title'] : null;
        $ch_widget_post_num   = isset($instance['ch-widget-post-number']) ? $instance['ch-widget-post-number'] : null ;

        if(isset($instance['ch-widget-post-is-allow-sticky'])){
              $check_content_st = 'checked="checked"';
        }else{
            $check_content_st = "";
        }
         
       ?>
        <p>
          <label for="<?php echo esc_attr($this->get_field_id('ch-widget-title') ); ?>"><?php esc_html_e( 'Widget Title:','niroon' ); ?></label>
          <input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'ch-widget-title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'ch-widget-title' )); ?>" type="text" value = '<?php echo esc_attr($ch_widget_title); ?>'/>
        </p>

        <p>
          <label for="<?php echo esc_attr($this->get_field_id( 'ch-widget-post-number') ); ?>"><?php esc_html_e( 'Post Number:','niroon' ); ?></label>
          <input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'ch-widget-post-number' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'ch-widget-post-number' )); ?>" type="number" min = "0" value = '<?php echo esc_attr($ch_widget_post_num); ?>'/>
        </p>

        <p>
          <label for="<?php echo esc_attr($this->get_field_id( 'ch-widget-post-is-allow-sticky') ); ?>"></label>
          <input  type="checkbox"  class="widefat" id="<?php echo esc_attr($this->get_field_id( 'ch-widget-post-is-allow-sticky' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'ch-widget-post-is-allow-sticky' )); ?>" <?php echo $check_content_st; ?> ><?php esc_html_e('Show Sticky Posts','niroon'); ?></input>
        </p>
       <?php
    }
	public function update( $new_instance, $old_instance ) {
        $old_instance['ch-widget-title'] = $new_instance['ch-widget-title'];
        $old_instance['ch-widget-post-number'] = $new_instance['ch-widget-post-number'];
        $old_instance['ch-widget-post-is-allow-sticky'] = $new_instance['ch-widget-post-is-allow-sticky'];
        return $old_instance;
	}
}
add_action( 'widgets_init', function(){ register_widget( 'CH_posts_widget' ); });
?>